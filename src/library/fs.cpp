// fs.cpp: File System

#include "sfs/fs.h"

#include <algorithm>

#include <assert.h>
#include <stdio.h>
#include <string.h>

// Debug file system -----------------------------------------------------------

void FileSystem::debug(Disk *disk) {
    Block block;

    // Read Superblock
    disk->read(0, block.Data);

    printf("SuperBlock:\n");
    //Check if valid
    if (block.Super.MagicNumber == MAGIC_NUMBER) {
        printf("    magic number is valid\n");
    }else{
        printf("    magic number is not valid\n");
    }

    printf("    %u blocks\n"         , block.Super.Blocks);
    printf("    %u inode blocks\n"   , block.Super.InodeBlocks);
    printf("    %u inodes\n"         , block.Super.Inodes);

    // Read Inode blocks
    Block inode_block;
    // Loop through inode blocks
    for (uint32_t i = 1; i <= block.Super.InodeBlocks; i++) { 
        disk->read(i, inode_block.Data);

        // Loop through inodes
        for (uint32_t j = 0; j < INODES_PER_BLOCK; j++) {  
            Inode inode = inode_block.Inodes[j];
            if (inode.Valid) {
                printf("\nInode %d:\n", j);
                printf("    size: %u bytes\n", inode.Size);
                printf("    direct blocks:");

                // Loop through direct pointers
                for (uint32_t k = 0; k < POINTERS_PER_INODE; k++) { 
                    if (inode.Direct[k]) {
                        printf(" %lu", (unsigned long)(inode.Direct[k]));
                    }
                }
                if (inode.Indirect) {  
                    printf("\n    indirect block: %lu\n", (unsigned long)(inode.Indirect));
                    printf("    indirect data blocks:");

                    Block indirect_block;
                    disk->read(inode.Indirect, indirect_block.Data);
                    // Loop through indirect pointers
                    for (uint32_t l = 0; l < POINTERS_PER_BLOCK; l++) {
                        if (indirect_block.Pointers[l]){
                            printf(" %d",(indirect_block.Pointers[l]));
                        }
                    }
                } 
            }
        }
    }

}

// Format file system ----------------------------------------------------------

bool FileSystem::format(Disk *disk) {
    // Make sure disk is mounted
    if (disk->mounted())
        return false;

    // Write superblock 
    Block block;
    block.Super.MagicNumber = MAGIC_NUMBER;
    block.Super.Blocks = disk->size();
    block.Super.InodeBlocks = std::ceil(disk->size() * 0.10); //Use 10% as inode blocks
    block.Super.Inodes = block.Super.InodeBlocks * INODES_PER_BLOCK;

    disk->write(0, block.Data);

    // Format everything

    Block new_block;

    for (size_t i = 0; i < Disk::BLOCK_SIZE; i++) {
        new_block.Data[i] = 0;
    }
    for (size_t j = 1; j < disk->size(); j++) {
        disk->write(j, new_block.Data);
    }   
    
    return true;

}

// Mount file system -----------------------------------------------------------

bool FileSystem::mount(Disk *disk) {
    if (disk->mounted())
        return false;

    // Read superblock
    Block block;
    disk->read(0, block.Data);

    // Ensure proper values
    if (block.Super.MagicNumber != MAGIC_NUMBER) {
        return false;
    }
    if (block.Super.Blocks != disk->size()) {
        return false;
    }
    if (block.Super.InodeBlocks != std::ceil(block.Super.Blocks * 0.10)) {
        return false;
    }
    if (block.Super.Inodes != block.Super.InodeBlocks * INODES_PER_BLOCK) {
        return false;
    }

    // Set device and mount
    Device = disk;
    Device->mount();

    // Copy metadata 
    FS_superblock = block.Super;

    // Allocate free block bitmap 
    for (uint32_t i = 1; i <= block.Super.InodeBlocks; i++) {  
        disk->read(i, block.Data);
        for (uint32_t j = 0; j < INODES_PER_BLOCK; j++) {
            Inode inode = block.Inodes[j];
            if (inode.Valid) {
                for (uint32_t k = 0; k < POINTERS_PER_INODE; k++) { 
                    if (inode.Direct[k]) {
                        free_blocks.emplace(inode.Direct[k], true);
                    }
                }
                if (inode.Indirect) { 
                    free_blocks.emplace(inode.Indirect, true);
                    Block indirect_block;
                    disk->read(inode.Indirect, indirect_block.Data);
                    for (uint32_t h = 0; h < POINTERS_PER_BLOCK; h++) { 
                        if (indirect_block.Pointers[h]) {
                            free_blocks.emplace(indirect_block.Pointers[h], true);
                        }
                    }
                } 
            }
        }
    }

    return true;
}

// Create inode ----------------------------------------------------------------

ssize_t FileSystem::create() {
    // Locate free inode in inode table
    Block block;
    for (uint32_t i = 1; i <= FS_superblock.InodeBlocks; i++) {
        Device->read(i, block.Data);
        for (uint32_t j = 0; j < INODES_PER_BLOCK; j++) { 
            int base = (i-1) * INODES_PER_BLOCK;
            int offset = j;
            // Locate free inode in inode table
            if (!block.Inodes[j].Valid) {
                block.Inodes[j].Valid = 1;
                Device->write(i, block.Data);
                return (base + offset);
            }
        }
    }
    return -1;
}

// Remove inode ----------------------------------------------------------------

bool FileSystem::remove(size_t inumber) {
    // Load inode information
    Block block;
    for (uint32_t i = 1; i <= FS_superblock.InodeBlocks; i++) {
        Device->read(i, block.Data);
        for (uint32_t j = 0; j < INODES_PER_BLOCK; j++) {
            int base = (i-1) * INODES_PER_BLOCK;
            int offset = j;
            Inode inode = block.Inodes[j];
            if (inumber == base + offset) { 
                if (!block.Inodes[j].Valid) { 
                    return false; 
                }
                block.Inodes[j].Valid = 0;
                Device->write(i, block.Data);

                // Free direct blocks
                if (inode.Direct[j]) {
                    for (uint32_t k = 0; k < POINTERS_PER_INODE; k++) { 
                        free_blocks.emplace(inode.Direct[k], 0);
                        block.Inodes[j].Direct[k] = 0;
                    }
                }

                // Free indirect blocks
                if (inode.Indirect) {
                    // Clear inode in inode table
                    free_blocks.emplace(inode.Indirect, 0);
                    Block indirect_block;
                    Device->read(inode.Indirect, indirect_block.Data);
                    for (uint32_t h = 0; h < POINTERS_PER_BLOCK; h++) { 
                        if (indirect_block.Pointers[h]) {
                            free_blocks.emplace(indirect_block.Pointers[h], 0);
                            indirect_block.Pointers[h] = 0; 
                        }
                    }
                }
            }
        }
    }
    return true;
}

// Inode stat ------------------------------------------------------------------

ssize_t FileSystem::stat(size_t inumber) {
    // Load inode information
    Block block;
    for (uint32_t i = 1; i <= FS_superblock.InodeBlocks; i++) {
        Device->read(i, block.Data);
        for (uint32_t j = 0; j < INODES_PER_BLOCK; j++) {
            int base = (i-1) * INODES_PER_BLOCK;
            int offset = j;
            Inode inode = block.Inodes[j];
            if (inumber == base + offset) {
                if (block.Inodes[j].Valid)
                    return inode.Size;
                else
                    return -1;
            }
        }
    }
    return -1;

}

// Read from inode -------------------------------------------------------------

ssize_t FileSystem::read(size_t inumber, char *data, size_t length, size_t offset) {
    // Load inode information
    Block block;
    for (uint32_t i = 1; i <= FS_superblock.InodeBlocks; i++) { 
        Device->read(i, block.Data);
        for (uint32_t j = 0; j < INODES_PER_BLOCK; j++) {
            int curr = (i-1) * INODES_PER_BLOCK + j;
            Inode inode = block.Inodes[j];
            if (inumber == curr) {
                if (inode.Valid) {
                    // Adjust length
                    uint32_t adjusted_length = length;
                    if ((inode.Size - offset) < length) {
                        adjusted_length = (inode.Size - offset);
                    }

                    // Read block and copy to data
                    Block read_block;
                    size_t index = std::floor(offset / Disk::BLOCK_SIZE);
                    printf("offset: %lu\n", offset);
                    printf("Disk::BLOCK_SIZE: %lu\n", Disk::BLOCK_SIZE);
                    printf("index: %lu\n", index);
      
                    if (index < POINTERS_PER_INODE) { 
                        Device->read(inode.Direct[index], read_block.Data);
                        memcpy(data, read_block.Data + (offset % Disk::BLOCK_SIZE), adjusted_length);
                        return adjusted_length;
                    } else { 
                        if (inode.Indirect) {
                            Block indirect_block;
                            Device->read(inode.Indirect, indirect_block.Data);
                            Device->read(indirect_block.Pointers[index - POINTERS_PER_INODE], read_block.Data);
                            memcpy(data, read_block.Data + (offset % Disk::BLOCK_SIZE), adjusted_length);
                            return adjusted_length;
                        } else {
                            return -1;
                        }
                    }
                } else {
                    return -1;
                }
            } 
        }
    }
    return -1;
}

// Write to inode --------------------------------------------------------------

ssize_t FileSystem::write(size_t inumber, char *data, size_t length, size_t offset) {
    // Load inode
    Block block;
    for (uint32_t i = 1; i <= FS_superblock.InodeBlocks; i++) {
        Device->read(i, block.Data);
        for (uint32_t j = 0; j < INODES_PER_BLOCK; j++) {
            int curr = (i-1) * INODES_PER_BLOCK + j; 
            Inode inode = block.Inodes[j];
            if (inumber == curr) {
                if (inode.Valid) {
                    // Adjust length
                    uint32_t adjusted_length = length;
                    if ((inode.Size - offset) < length) {
                        adjusted_length = (inode.Size - offset);
                    }

                    // Write block and copy to data
                    Block write_block;
                    size_t index = std::floor(offset / Disk::BLOCK_SIZE);
                    if (index < POINTERS_PER_INODE) {
                        Device->read(inode.Direct[index], write_block.Data);
                    }
    
                    return adjusted_length;
                } else {
                    return -1;
                }
            }
        }
    }
    return -1;

}
